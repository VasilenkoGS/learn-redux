import {ADD_TODO} from '../const/todo'

export const todos = (state = [], action) => {
    switch (action.type) {
        case ADD_TODO:
            return [...state, {
                id: state.length + 1,
                title: action.title,
                done: false
            }];
        case TOGGLE_TODO:
            return state.map(todo => { 
                if (todo.id === action.id) {
                    return {...todo, done: !todo.done};
                }
                return todo;
            })
        default: return state;
    }
    
};


