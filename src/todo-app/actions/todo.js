import {ADD_TODO} from '../const/todo';

export const addTodo = (title) => ({
    type: ADD_TODO,
    title
});